package com.hlj.malltiny.dao;

import com.hlj.malltiny.mbg.model.UmsPermission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：后台用户与角色管理自定义Dao
 *
 * @author haolijun at 2020/9/3 10:23
 * @version 1.0.0
 */
public interface UmsAdminRoleRelationDao {
    /**
     * 获取用户所有权限(包括+-权限)
     */
    List<UmsPermission> getPermissionList(@Param("adminId") Long adminId);
}
