package com.hlj.malltiny.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 描述：定時配置任務
 *
 * @author haolijun at 2020/9/3 17:30
 * @version 1.0.0
 */
@Configuration
@EnableScheduling
public class SpringTaskConfig {
}
