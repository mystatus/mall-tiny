package com.hlj.malltiny.controller;

import com.hlj.malltiny.service.QnUploadService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * 描述：
 *
 * @author haolijun at 2020/9/7 15:05
 * @version 1.0.0
 */
@Controller
@Api(tags = "QnUploadController",description = "七牛云上传")
@RequestMapping("/oss")
public class QnUploadController {
    @Autowired
    private QnUploadService qnUploadService;

    @ApiOperation("上传")
    @PostMapping(value = "/image")
    public  String upLoadImage(@RequestParam("file") MultipartFile file) throws IOException {

        // 获取文件的名称
        String fileName = file.getOriginalFilename();

        // 使用工具类根据上传文件生成唯一图片名称
        String imgName = "test.jpg";

        if (!file.isEmpty()) {

            FileInputStream inputStream = (FileInputStream) file.getInputStream();

            String path = qnUploadService.uploadFile(inputStream,imgName);
            System.out.print("七牛云返回的图片链接:" + path);
            return path;
        }
        return "上传失败";
    }
    @ApiOperation("删除")
    @PostMapping(value = "/delete")
    @ResponseBody
    public  String deleteImage(@RequestParam("file") MultipartFile file) throws IOException {
        return qnUploadService.delete(file.getName());
    }
}
