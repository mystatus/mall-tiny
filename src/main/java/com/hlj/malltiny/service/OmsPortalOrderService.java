package com.hlj.malltiny.service;

import com.hlj.malltiny.common.api.CommonResult;
import com.hlj.malltiny.dto.OrderParam;
import org.springframework.transaction.annotation.Transactional;

/**
 * 描述：
 *
 * @author haolijun at 2020/9/6 19:05
 * @version 1.0.0
 */
public interface OmsPortalOrderService {

    /**
     * 根据提交信息生成订单
     */
    @Transactional
    CommonResult generateOrder(OrderParam orderParam);

    /**
     * 取消单个超时订单
     */
    @Transactional
    void cancelOrder(Long orderId);
}
