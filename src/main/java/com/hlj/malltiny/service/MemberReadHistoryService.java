package com.hlj.malltiny.service;

import com.hlj.malltiny.nosql.mongodb.document.MemberReadHistory;

import java.util.List;

/**
 * 描述：会员浏览记录管理Service
 *
 * @author haolijun at 2020/9/4 17:09
 * @version 1.0.0
 */
public interface MemberReadHistoryService {
    /**
     * 生成浏览记录
     */
    int create(MemberReadHistory memberReadHistory);

    /**
     * 批量删除浏览记录
     */
    int delete(List<String> ids);

    /**
     * 获取用户浏览历史记录
     */
    List<MemberReadHistory> list(Long memberId);
}
