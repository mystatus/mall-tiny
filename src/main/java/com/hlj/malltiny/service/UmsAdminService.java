package com.hlj.malltiny.service;

import com.hlj.malltiny.mbg.model.UmsAdmin;
import com.hlj.malltiny.mbg.model.UmsPermission;

import java.util.List;

/**
 * 描述：后台管理员Service
 *
 * @author haolijun at 2020/9/3 10:03
 * @version 1.0.0
 */
public interface UmsAdminService {
    /**
     * 根据用户名获取后台管理员
     */
    UmsAdmin getAdminByUsername(String username);

    /**
     * 注册功能
     */
    UmsAdmin register(UmsAdmin umsAdminParam);

    /**
     * 登录功能
     * @param username 用户名
     * @param password 密码
     * @return 生成的JWT的token
     */
    String login(String username, String password);

    /**
     * 获取用户所有权限（包括角色权限和+-权限）
     */
    List<UmsPermission> getPermissionList(Long adminId);
}
