package com.hlj.malltiny.service;

import com.qiniu.common.QiniuException;

import java.io.File;
import java.io.InputStream;

/**
 * 描述：
 *
 * @author haolijun at 2020/9/7 15:01
 * @version 1.0.0
 */
public interface QnUploadService {
    public String uploadFile(InputStream inputStream, String fileName) throws QiniuException;
    public String delete(String key) throws QiniuException;
    public String uploadFile(File file, String fileName) throws QiniuException;
}
