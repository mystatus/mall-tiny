package com.hlj.malltiny.nosql.mongodb.respository;

import com.hlj.malltiny.nosql.mongodb.document.MemberReadHistory;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * 描述：会员商品浏览历史Repository
 *
 * @author haolijun at 2020/9/4 16:55
 * @version 1.0.0
 */
public interface MemberReadHistoryRepository extends MongoRepository<MemberReadHistory,String> {
    /**
     * 根据会员id按时间倒序获取浏览记录
     * @param memberId 会员id
     */
    @Query("{ 'memberId' : ?0 }")
    List<MemberReadHistory> findByMemberIdOrderByCreateTimeDesc(Long memberId);

    /**
     * 根据会员id按时间倒序获取浏览记录
     * @param memberId
     * @return
     */
    List<MemberReadHistory> findAllByMemberIdOrderByCreateTimeDesc(Long memberId);

}
